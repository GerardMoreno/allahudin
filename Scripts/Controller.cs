﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Controller : MonoBehaviour {

    public float MaxS = 10;
    public float Acceleration = 35;
    public float JumpS = 8;
    public float JumpTime;

    public bool EnableDoubleJump = true;

    public bool wallHitDoubleJumpOverride = true;

    bool canDoubleJump = true;

    float jTime;

    bool jumpKeyDown = false;
    bool canVariableJump = false;



    private Rigidbody2D rb2d = null;
    private Animator anim;

    private bool flipped = false;
    private bool muerto = false;

    private Vector3 origen;

    public AudioClip jumpSound;
    public AudioClip dolor;

    private AudioSource source;


    void Awake()
    {
        // Obtenemos el rigidbody y lo guardamos en la variable rb2d
        // para poder utilizarla más cómodamente
        rb2d = GetComponent<Rigidbody2D>();

        // Obtenemos el Animator Controller para poder modificar sus variables
        anim = GetComponent<Animator>();

        //Guardamos la posición original para reiniciar
        origen = this.transform.position;

        source = GetComponent<AudioSource>();

    }

    // Update is called once per frame
    void FixedUpdate() {

        if (muerto) { Restart(); }

        //Movimiento psg like meat boyy

        float horizontal = Input.GetAxis("Horizontal");

        if (horizontal < -0.1f)
        {
            if (rb2d.velocity.x > -this.MaxS)
            {
                rb2d.AddForce(new Vector2(-this.Acceleration, 0.0f));
            }
            else
            {
                rb2d.velocity = new Vector2(-this.MaxS, rb2d.velocity.y);
            }
        }

        else if (horizontal > 0.1f)
        {
            if (rb2d.velocity.x < this.MaxS)
            {
                rb2d.AddForce(new Vector2(this.Acceleration, 0.0f));
            }
            else
            {
                rb2d.velocity = new Vector2(this.MaxS, rb2d.velocity.y);
            }
        }

        bool Grounded = isGrounded();

        float vertical = Input.GetAxis("Jump");

        if (Grounded)
        {
            canDoubleJump = true;
        }
        if (vertical > 0.1f)
        {

            if (!jumpKeyDown)
            {
                jumpKeyDown = true;

                if (Grounded || (canDoubleJump && EnableDoubleJump) || wallHitDoubleJumpOverride)
                {

                    bool wallHit = false;
                    int wallHitDirection = 0;

                    bool leftWallHit = isOnWallLeft();
                    bool rightWallHit = isOnWallRight();

                    if (horizontal != 0)
                    {
                        if (leftWallHit)
                        {
                            wallHit = true;
                            wallHitDirection = 1;
                        }
                        else if (rightWallHit)
                        {
                            wallHit = true;
                            wallHitDirection = -1;
                        }
                    }

                    if (!wallHit)
                    {
                        if (Grounded || (canDoubleJump && EnableDoubleJump))
                        {

                            source.PlayOneShot(jumpSound);
                            rb2d.velocity = new Vector2(rb2d.velocity.x, this.JumpS);
                            jTime = 0.0f;
                            canVariableJump = true;

                        }
                    }
                    else
                    {
                        source.PlayOneShot(jumpSound);
                        rb2d.velocity = new Vector2(this.JumpS * wallHitDirection, this.JumpS);

                        jTime = 0.0f;
                        canVariableJump = true;
                    }

                    if (!Grounded && !wallHit)
                    {
                        canDoubleJump = false;
                    }

                }// frame 2

                else if (canVariableJump)

                {
                    jTime += Time.deltaTime;

                    if (jTime < this.JumpTime / 1000)
                    {
                        rb2d.velocity = new Vector2(rb2d.velocity.x, this.JumpS);
                    }
                }
            }
        }

        else
        {
            jumpKeyDown = false;
            canVariableJump = false;
        }



        //animaciones

        if (rb2d.velocity.x > 0.1f || rb2d.velocity.x < -0.1f)
        {
            if ((rb2d.velocity.x < -0.1f && flipped) || (rb2d.velocity.x > -0.1f && !flipped))
            {
                flipped = !flipped;
                this.transform.rotation = Quaternion.Euler(0, flipped ? 180 : 0, 0);
            }
            anim.SetBool("walking", true);
        }

        else
        {
            anim.SetBool("walking", false);
        }

        if (!Grounded)
        {
            anim.SetBool("jumping", true);
        }

        else
        {
            anim.SetBool("jumping", false);
        }


    }


    private bool isGrounded()
    {
        float lenghtToSearch = 0.1f;
        float colliderTreshold = 0.001f;

        Vector2 lineStart = new Vector2(this.transform.position.x, this.transform.position.y - this.GetComponent<Renderer>().bounds.extents.y - colliderTreshold);

        Vector2 vectorToSearch = new Vector2(this.transform.position.x, lineStart.y - lenghtToSearch);

        RaycastHit2D hit = Physics2D.Linecast(lineStart, vectorToSearch);


        return hit;
    }

    private bool isOnWallLeft()
    {
        bool retVal = false;

        float lenghtToSearch = 0.1f;
        float colliderTreshold = 0.01f;

        Vector2 lineStart = new Vector2(this.transform.position.x - this.GetComponent<Renderer>().bounds.extents.x - colliderTreshold, this.transform.position.y);

        Vector2 vectorToSearch = new Vector2(lineStart.x - lenghtToSearch, this.transform.position.y);

        RaycastHit2D hitLeft = Physics2D.Linecast(lineStart, vectorToSearch);

        retVal = hitLeft;

        if (retVal)
        {

            if (hitLeft.collider.GetComponent<NoSlideJump>()) { retVal = false; }

        }

        return retVal;
    }



         private bool isOnWallRight()
    {
        bool retVal = false;

        float lenghtToSearch = 0.1f;
        float colliderTreshold = 0.01f;

        Vector2 lineStart = new Vector2(this.transform.position.x + this.GetComponent<Renderer>().bounds.extents.x + colliderTreshold, this.transform.position.y);

        Vector2 vectorToSearch = new Vector2(lineStart.x + lenghtToSearch, this.transform.position.y);

        RaycastHit2D hitRight = Physics2D.Linecast(lineStart, vectorToSearch);

        retVal = hitRight;

        if (retVal)
        {

            if (hitRight.collider.GetComponent<NoSlideJump>()) { retVal = false; }

        }

        return retVal;

    }

    //Choques
    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "muerte")
        {
            source.PlayOneShot(dolor);
            muerto = true;
            anim.SetBool("dead", true);
        }


        else if (coll.gameObject.tag == "nivel1")
        {
            SceneManager.LoadScene("nivel2");
        }

        else if (coll.gameObject.tag == "nivel2")
        {
            SceneManager.LoadScene("nivel3");
        }

        else if (coll.gameObject.tag == "nivel3")
        {
            SceneManager.LoadScene("final");
        }
    }

        void onCollisionExit2D(Collision2D other)
        {            
        if (other.transform.tag == "movingplatform")
            {
                transform.parent = null;
            }
        }

    void Restart()
    {
        this.transform.position = origen;
        anim.SetBool("dead", false);
        muerto = false;
        anim.Play("idle");
    }

}
