﻿using UnityEngine;
using System.Collections;

public class Pendulum : MonoBehaviour {

    //Las tenemos para controlar el pendulo
   
    public float leftPushRange;
    public float rightPushRange;
    public float velocityTreshold;


    private Rigidbody2D rb2d = null;



    // Use this for initialization
    void Start () {

        rb2d = GetComponent<Rigidbody2D>();
        rb2d.angularVelocity = velocityTreshold;

	}
	
	// Update is called once per frame
	void Update () {

        if (transform.rotation.z > 0 && transform.rotation.z < rightPushRange && (rb2d.angularVelocity > 0) && rb2d.angularVelocity < velocityTreshold)
        {
            rb2d.angularVelocity = velocityTreshold; //Swings to one side
        }
        else if (transform.rotation.z < 0 && transform.rotation.z > leftPushRange && (rb2d.angularVelocity < 0) && rb2d.angularVelocity > velocityTreshold * -1)
        {
            rb2d.angularVelocity = velocityTreshold * -1; //Swings to te other side
        }


    }
}
